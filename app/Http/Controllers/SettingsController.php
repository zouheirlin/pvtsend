<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Queue;

class SettingsController extends Controller
{
    function getSettings()
    {
        $email = env('EMAIL_DEBUG');
        return response()->json(['email' => $email]);
    }

    function updateEmail(Request $request)
    {
        $this->updateDotEnv('EMAIL_DEBUG', $request->email);
        return response()->json(['email' => $request->email, 'message' => 'Debug email saved!']);
    }

    public function updateDotEnv($key, $newValue, $delim=''){

	    $path = base_path('.env');
	    $oldValue = env($key);

	    if ($oldValue === $newValue) {
	        return;
	    }

	    if (file_exists($path)) {
	        file_put_contents(
	            $path, str_replace(
	                $key.'='.$delim.$oldValue.$delim, 
	                $key.'='.$delim.$newValue.$delim, 
	                file_get_contents($path)
	            )
	        );
	    }
    }

    public function checkUser(){
        $user = User::all();
        return [$user];
    }

    public function createUser(Request $request){
        $user = new User();
        $user->name = "admin";
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
		$user->save();
		return '1';
    }
    
    public function test(){
        $size = Queue::size();
        echo $size;
    }
}
